import React from 'react';

const ReviewItems = (props) => {
    // console.log(props);
    const {name, img, quantity,key, price} = props.cartProd;
    const removeItem = props.removeItem;
    return (
        <div>
            <br/>
            <div className="product">
            
            <div>
                <img src={img} alt=""/>
            </div>
            <div className="product-name">
                <h4>{name}</h4>
                <p><small>Price: ${price}</small></p>
                <p><b>Quantity: {quantity}</b></p>
                <br/><br/><br/><br/><br/>
                <button className="addtocartbtn" onClick={() => removeItem(key)}>Remove Item</button>
            </div>
            </div>
        </div>
        
    );
};

export default ReviewItems;