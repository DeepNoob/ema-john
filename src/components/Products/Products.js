import React from 'react';
import './Products.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee, faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import {Link} from 'react-router-dom'
const Products = (props) => {
    const {name, img, price, seller,stock, key} = props.products;
    const fromDetails = props.fromDetails;
    return (
        <div className="product">
            <div>
                <img src={img} alt=""/>
            </div>
            <div className="product-name">
                <h4><Link to={"/product/"+key}>{name}</Link></h4>
                <br/>
                <p><small>By: {seller}</small></p>
                <p>${price}</p>
                <p>Only {stock} is left- Order Soon</p>
                {!fromDetails && 
                    <button className="addtocartbtn" onClick={() => props.handleAddProduct(props.products)}>
                        <FontAwesomeIcon icon={faShoppingCart} /> Add to cart
                    </button>
                }
            </div>
        </div>
    );
};

export default Products;