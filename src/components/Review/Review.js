import React, { useEffect, useState } from 'react';
import { getDatabaseCart, processOrder, removeFromDatabaseCart } from '../../utilities/databaseManager';
import fakeData from '../../fakeData/index'
import ReviewItems from '../ReviewItems/ReviewItems';
import Cart from '../Cart/Cart';
import thank from '../../images/giphy.gif'

const Review = () => {
    const [cart, setCart] = useState([])
    useEffect(()=>{
        const savedProduct = getDatabaseCart();
        const productKeys = Object.keys(savedProduct);
        
        const cartProduct = productKeys.map(key => {
            const product = fakeData.find(pd => pd.key === key);
            product.quantity = savedProduct[key]; 
            return product;
        })
        setCart(cartProduct);
    },[])

    const [orderPlaced, setOrderPaced] = useState(false)

    ;

    const finalOrderHandle = () => {
        // console.log("Handeled");
        processOrder();
        setCart([]);
        setOrderPaced(true)
    }

    let thanks;
    if(orderPlaced){
        thanks = <img src={thank}></img>
    }

    const removeItem = (productKey) => {
        const newCart = cart.filter(pd => pd.key != productKey)
        setCart(newCart);
        removeFromDatabaseCart(productKey);
        // console.log("Removed");
    }

    return (
        <div className="main-container">
            <div className="product-container">
                <h1>Cart Item: {cart.length}</h1>
                {
                    cart.map(crtProd => <ReviewItems cartProd={crtProd} key={crtProd.key} removeItem={removeItem}></ReviewItems>)
                }
                { thanks }
            </div>
            
            <div className="cart-container">
                <Cart cart={cart}>
                    <button onClick={finalOrderHandle} className="addtocartbtn">Place Order</button>
                </Cart>
                
            </div>
            
            
        </div>
    );
};

export default Review;