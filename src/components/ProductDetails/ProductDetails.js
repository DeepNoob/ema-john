import React from 'react';
import { useParams } from 'react-router';
import fakeData from '../../fakeData';
import Products from '../Products/Products';

const ProductDetails = () => {
    let { productKey } = useParams();
    // console.log(productKey);
    const product = fakeData.find(pd => productKey === pd.key)
    // console.log(product);
    return (
        <div>
            <h2>Coming Soon</h2>
            <Products products = {product} fromDetails = {true}></Products>
        </div>
    );
};

export default ProductDetails;