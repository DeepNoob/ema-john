import React, { useEffect, useState } from 'react';
import fakedata from '../../fakeData';
import Cart from '../Cart/Cart';
import Products from '../Products/Products';
import './Shop.css';
import {addToDatabaseCart, getDatabaseCart} from '../../utilities/databaseManager';
import { Link } from 'react-router-dom';

const Shop = () => {

    const first10 = fakedata.slice(0,10)
    const [products, setProducts] = useState(first10) 
    const [cart, setCart] = useState([])

    useEffect(() => {
        const savedCart = getDatabaseCart();
        const productKeys = Object.keys(savedCart);
        const previousCart = productKeys.map(existingKey => {
            const product = fakedata.find(pd => pd.key === existingKey);
            product.quantity = savedCart[existingKey];
            return product;
        })
        setCart(previousCart);
    }, [])

    const handleAddProduct = (product) => {
        // console.log("Product added", product);
        const toBeAddedKey = product.key;
        const sameProduct = cart.find(pd => pd.key === toBeAddedKey);
        let count = 1;
        let newCart;
        if(sameProduct){
            count = sameProduct.quantity + 1;
            sameProduct.quantity = count;
            const others = cart.filter(pd => pd.key !== toBeAddedKey);
            newCart = [...others, sameProduct]
        }
        else{
            product.quantity = 1;
            newCart = [...cart, product];
        }
        // const count = sameProduct.length;
        // const newCart = [...cart, product];
        setCart(newCart);
        // const samePd = newCart.filter(pd => product.key === pd.key);
        // const itemCount = samePd.length;
        addToDatabaseCart(product.key, count);
    }
    return (
        <div className="main-container">
            <div className="product-container">
                {
                    products.map(pd => <Products products = {pd} key={pd.key} fromDetails={false} handleAddProduct = {handleAddProduct}></Products>)
                }
            </div>
            <div className="cart-container">
                <Cart cart={cart}>
                    <Link to="/review">
                        <button className="addtocartbtn">Order Review</button>
                    </Link>
                </Cart>
                
            </div>
        </div>
    );
};

export default Shop;